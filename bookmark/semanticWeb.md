### Table of Contents
---
  0. [w3.org standards](https://www.w3.org/standards/semanticweb/)
  0. [Semanticweb.org wiki](http://semanticweb.org/wiki/Main_Page.html)


### Intro
  0. [Cambridge Intro about Semantic Web](http://www.cambridgesemantics.com/semantic-university/introduction-semantic-web)
